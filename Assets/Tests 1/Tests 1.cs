using NUnit.Framework;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;

public class tests1
{
    [UnityTest]
    public IEnumerator CanGrabCoins()
    {
        // Loading the scene
        SceneManager.LoadScene(0);


        // Let Unity Load
        yield return new WaitForSeconds(1f);


        // Get needed components
        var player = GameObject.Find("Player");
        var laneSwitching = player.GetComponent<LaneSwitching>();

        // Wait for the game to start
        yield return new WaitForSeconds(3f);

        // Asserting amount of coins is 0
        Assert.IsTrue(LevelManager.instance.pickUps == 0, "yay");

        // Move to the right
        laneSwitching.MoveLane(LaneSwitch.Right);

        // Wait for the game to play
        yield return new WaitForSeconds(7f);

        Assert.IsTrue(LevelManager.instance.pickUps == 3);
    }

    [UnityTest]
    public IEnumerator TestIfLevelResets()
    {
        // Loading the scene
        SceneManager.LoadScene(0);

        // Let Unity Load
        yield return new WaitForSeconds(1f);

        // Get needed components
        LevelManager.instance.amountOfLives = 1;

        // Wait for the game to start
        yield return new WaitForSeconds(3f);

        // Let the game play
        yield return new WaitForSeconds(5f);

        Assert.IsTrue(LevelManager.instance.amountOfLives == 3);
    }
}
