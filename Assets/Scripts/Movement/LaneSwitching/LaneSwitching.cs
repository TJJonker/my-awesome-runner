using UnityEngine;

public enum LaneSwitch { Left, Right };

public class LaneSwitching : MonoBehaviour
{
    [Header("Switching Lanes")]
    [SerializeField] private float timeToSwitchLanes = .5f;
    [SerializeField] private float distanceBetweenLanes;

    private int lane = 0;
    private float desiredPosition;

    private float startTime;

    private float originalX;

    public void MoveLane(LaneSwitch laneSwitch)
    {
        originalX = transform.position.x;

        if (laneSwitch == LaneSwitch.Left && lane != -1) lane += -1;
        if (laneSwitch == LaneSwitch.Right && lane != 1) lane += 1;
        desiredPosition = distanceBetweenLanes * lane;

        startTime = Time.time;
    }

    private void Update()
    {
        float t = (Time.time - startTime) / timeToSwitchLanes;
        var desiredPos = new Vector3(Mathf.SmoothStep(originalX, desiredPosition, t), transform.position.y, transform.position.z);
        transform.position = desiredPos;
    }
}
