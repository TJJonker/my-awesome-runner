using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ForwardMovement : MonoBehaviour
{
    [Header("Forward Movement")]
    [SerializeField] private float startSpeed = 5;
    [SerializeField] private float slowDownSpeed = 3;
    [SerializeField] private float slowingDownTime = 3;

    private float speed;
    private Coroutine slowingDown;

    private Rigidbody rb;

    private void Awake() => rb = GetComponent<Rigidbody>();

    private void Start() => speed = startSpeed;

    private void FixedUpdate() => rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y, speed);

    public void SlowDown()
    {
        if(slowingDown != null) StopCoroutine(slowingDown);
        slowingDown = StartCoroutine(SlwDwn());
    }

    private IEnumerator SlwDwn()
    {
        speed = slowDownSpeed;
        yield return new WaitForSeconds(slowingDownTime);
        speed = startSpeed;
    }
}
