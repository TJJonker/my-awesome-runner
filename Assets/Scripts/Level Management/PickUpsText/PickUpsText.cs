using TMPro;
using UnityEngine;

public class PickUpsText : MonoBehaviour
{
    private TextMeshProUGUI text;

    private void Awake() => text = GetComponent<TextMeshProUGUI>();

    public void UpdateText(int pickUps) => text.text = "pickUps: " + pickUps;
}
