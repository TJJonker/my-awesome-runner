using UnityEngine;
using UnityEngine.SceneManagement;

public class ColissionManager : MonoBehaviour
{
    private ForwardMovement forwardMovement;

    private void Awake() => forwardMovement = GetComponent<ForwardMovement>();

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "PickUp") LevelManager.instance.PickUp();

        if (other.gameObject.tag == "Finish") SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        if (other.gameObject.tag == "Obstacle")
        {
            LevelManager.instance.LoseLife(); ;
            forwardMovement.SlowDown();
        }

    }
}
