using TMPro;
using UnityEngine;

public class DistanceText : MonoBehaviour
{
    private TextMeshProUGUI text;

    private void Awake() => text = GetComponent<TextMeshProUGUI>();

    public void UpdateText(int distance) => text.text = "Distance: " + distance;
}
