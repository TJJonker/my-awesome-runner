using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;
    private void Awake() => instance = this;

    [SerializeField] private LifeText lifeText;
    [SerializeField] private ScoreText scoreText;
    [SerializeField] private DistanceText distanceText;
    [SerializeField] private PickUpsText pickupsText;
    [SerializeField] private CountDownText countDownText;
    [SerializeField] private Transform player; 

    [SerializeField, Range(1, 5)] private int countDownlength = 3;
    [SerializeField, Range(1, 5)] public int amountOfLives = 3;

    [SerializeField] private int pointsPerCoin = 100;
    public int pickUps;

    [SerializeField] private ForwardMovement forwardMovement;
    [SerializeField] PlayerInput playerInput;
    private float countDown;

    private void Start()
    {
        countDown = countDownlength;
    }

    private void Update()
    {
        scoreText.UpdateText((int)player.position.z + pickUps * pointsPerCoin);
        distanceText.UpdateText((int)player.position.z);
        CountDown();
    }

    public void LoseLife()
    {
        // Subtract lives and update text
        amountOfLives--;
        lifeText.UpdateText(amountOfLives);

        // Restart level when died
        if (amountOfLives == 0) SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void PickUp()
    {
        pickUps++;
        pickupsText.UpdateText(pickUps);
    }

    private void CountDown()
    {
        countDown -= Time.deltaTime;
        countDownText.UpdateText((int)Mathf.Ceil(countDown));

        if (countDown <= 0)
        {
            countDownText.gameObject.SetActive(false);
            forwardMovement.enabled = true;
            playerInput.enabled = true;
        }
    }

}
