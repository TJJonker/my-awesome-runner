using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private Transform objectToFollow;
    [SerializeField] private Vector3 cameraOffset;
    [SerializeField] private float cameraSpeed;

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, objectToFollow.position + cameraOffset, cameraSpeed);
    }
}
