using UnityEngine;
using TMPro;

public class LifeText : MonoBehaviour
{
    private TextMeshProUGUI text;

    private void Awake() => text = GetComponent<TextMeshProUGUI>();

    public void UpdateText(int lives) => text.text = "x " + lives;
}
