using TMPro;
using UnityEngine;

public class CountDownText : MonoBehaviour
{
    private TextMeshProUGUI text;

    private void Awake() => text = GetComponent<TextMeshProUGUI>();

    public void UpdateText(int time) => text.text = time.ToString();
}
