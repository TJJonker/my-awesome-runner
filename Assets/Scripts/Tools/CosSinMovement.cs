using UnityEngine;

public class CosSinMovement : MonoBehaviour
{
    [SerializeField] private float amplitude = 1;
    [SerializeField] private float frequency = 1;

    private float sinX;

    private void Update()
    {
        transform.localPosition = transform.right * (Mathf.Sin(sinX * frequency) * amplitude); 

        sinX += Time.deltaTime;
    }
}
