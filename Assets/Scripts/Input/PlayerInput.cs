using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] private LaneSwitching laneSwitching;
    [SerializeField] private Jumping jumping;

    private void Update()
    {
        InputPlayer();
    }

    private void InputPlayer()
    {
        // Move Right
        if (Input.GetKeyDown(KeyCode.D)) laneSwitching.MoveLane(LaneSwitch.Right);
        // Move Left
        if (Input.GetKeyDown(KeyCode.A)) laneSwitching.MoveLane(LaneSwitch.Left);
        // Jump
        if (Input.GetKeyDown(KeyCode.Space)) jumping.Jump();
    }
}
