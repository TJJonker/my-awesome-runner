using UnityEditor;
using UnityEngine;

class BuildWindow64Player
{
    static void PerformBuild()
    {
        string[] scenes = { "Assets/Scenes/SampleScene.unity" };
        BuildPipeline.BuildPlayer(scenes, "./Builds/game.exe", BuildTarget.StandaloneWindows64, BuildOptions.None);
    }
}
